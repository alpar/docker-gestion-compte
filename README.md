docker-gestion-compte
=====================

This is a complete stack for running gestion-compte into Docker containers using docker-compose tool.

# Installation

First, clone this repository:

```bash
$ git clone https://gitlab.com/alpar/docker-gestion-compte.git
```

Next, copy the `.env.dist` file to `.env`:

```bash
$ cp .env.dist .env
```

Make sure you adjust the variables in the `.env` file.

Then, run:

```bash
$ make install
```

This will download the last version of gestion-compte, and run some
installation tasks in the containers.

After it is finished, start the containers:

```bash
$ docker-compose up -d
```

You are done, you can visit your gestion-compte application on the following URL: `http://localhost`

_Note :_ you can rebuild all Docker images by running:

```bash
$ docker-compose build
```

# How it works?

Here are the `docker-compose` built images:

* `db`: This is the MySQL database container (can be changed to postgresql or whatever in `docker-compose.yml` file),
* `php`: This is the PHP-FPM container including the application volume mounted on,
* `nginx`: This is the Nginx webserver container in which php volumes are mounted too,

This results in the following running containers:

```bash
> $ docker-compose ps
        Name                       Command               State              Ports
--------------------------------------------------------------------------------------------
dockersymfony_db_1      docker-entrypoint.sh mysqld      Up      0.0.0.0:3306->3306/tcp
dockersymfony_nginx_1   nginx                            Up      443/tcp, 0.0.0.0:80->80/tcp
dockersymfony_php_1     php-fpm7 -F                      Up      0.0.0.0:9001->9001/tcp
```

# Read logs

You can access Nginx and Symfony application logs in the following directories on your host machine:

* `logs/nginx`
* `logs/symfony`

# Code license

You are free to use the code in this repository under the terms of the 0-clause BSD license. LICENSE contains a copy of this license.

install:
#	Ensure the db folder doesn't exist
	@if test -d db; \
		then echo "The database seems to already exist (./db/)."; \
		echo "Installation aborted to avoid messing with an existing installation."; \
		exit 1; \
	fi;
#	Ensure the .env file exists
	@if test ! -f .env; \
		then echo "You must first copy the .env.dist file to .env:"; \
		echo ""; \
		echo "$ cp .env.dist .env"; \
		echo ""; \
		echo "Then you must edit the .env file to change the parameters."; \
		exit 1; \
	fi;
	@echo "Downloading the gestion-compte app..."
	git clone --branch alpar-master https://gitlab.com/alpar/gestion-compte.git symfony
	@echo "Starting containers..."
	docker-compose up -d
	@echo "Waiting for mysql to initialize (this can take several minutes)..."
#	We loop until the php container can connect to the mysql database
#	and the 'gestioncompte' database exists.
	@while test "`docker-compose exec php php -r "print_r(mysqli_query(mysqli_connect(getenv('GESTIONCOMPTE_DB_HOST'), getenv('GESTIONCOMPTE_DB_USER'), getenv('GESTIONCOMPTE_DB_PASSWORD')), 'SHOW DATABASES like \'gestioncompte\';')-> fetch_row()[0]);"`X" != 'gestioncompteX' ; do \
		sleep 3; \
	done;
	@echo "mysql ready."
	@echo "Initializing gestion-compte (this can take several minutes)..."
	docker-compose exec php composer install --no-interaction
	docker-compose exec php bin/console doctrine:schema:create
	docker-compose exec php php bin/console cache:clear --env=prod --no-debug
	docker-compose exec php chown -R nobody /var/www/symfony
	docker-compose exec php chown -R nobody /var/www/symfony/var/logs
	docker-compose down
	@echo "gestion-compte installed. Run 'docker-compose up -d'."
